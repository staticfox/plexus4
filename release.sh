#!/bin/bash

SERNO=`cat include/serno.h | cut -d'"' -f2`

make distcheck
if [ $? -ne 0 ] ; then
  echo "Error running distcheck"
  exit 1
fi

mv plexus-4.tar.gz plexus4-$SERNO.tar.gz

echo "Release is ready as plexus4-$SERNO.tar.gz"
