#include "plexus_test.h"

#include <jansson.h>
#include <setjmp.h>

extern json_t *plexus_upgrade;
extern jmp_buf plexus_upgrade_jmp;

static char tmp[] = "/tmp/plexus.upgrade.test.XXXXXX";

static void other()
{
  plexus_up_conf(RESOURCEDIR "/test2.conf");

  WAIT_FOR(hash_find_client("plexus4.") != NULL);
}

static void check_upgrade()
{
  struct Client *client = hash_find_client("test");
  ck_assert(client != NULL);

  client = hash_find_client("plexus4.2");
  ck_assert(client != NULL);

  ck_assert(hash_find_channel("#a"));
}

static void upgrade_target()
{
  /* target of the upgrade */
  struct stat statbuf;

  while (stat(tmp, &statbuf) || statbuf.st_size == 0)
  {
    // can't WAIT_FOR as no ircd exists yet
    usleep(1000);
  }

  FILE *f = fopen(tmp, "r");
  json_error_t err;
  json_t *json = json_loadf(f, 0, &err);
  fclose(f);

  ck_assert(json != NULL);

  plexus_up();

  upgrade_unserialize(json, 1);
  json_decref(json);

  check_upgrade();
}

static void prepare_server()
{
  // get server state into what we want to upgrade

  struct MaskItem *conf = find_matching_name_conf(CONF_SERVER, "plexus4.2", NULL, NULL, 0);
  ck_assert(conf != NULL);

  serv_connect(conf, NULL);

  WAIT_FOR(hash_find_client("plexus4.2") != NULL);

  // abuse migrating sflags to prevent servers exiting from closing it
  struct Client *otherserver = hash_find_client("plexus4.2");
  AddSFlag(otherserver, SERVER_FLAGS_MIGRATING_QUEUE_WRITE);
  AddSFlag(otherserver, SERVER_FLAGS_MIGRATING_BLOCK_READ);

  struct PlexusClient *client = client_register("test");

  irc_join(client, "#a");
  expect_message(client, client->client, "JOIN");
}

START_TEST(test_upgrade)
{
  mkstemp(tmp);

  plexus_fork(other);

  plexus_fork(upgrade_target);

  plexus_up();

  prepare_server();

  if (!setjmp(plexus_upgrade_jmp))
    server_upgrade();

  FILE *f = fopen(tmp, "w");
  int ret = json_dumpf(plexus_upgrade, f, JSON_COMPACT | JSON_ENSURE_ASCII);
  fclose(f);

  ck_assert(ret == 0);

  plexus_join_all_and_exit();
}
END_TEST

void upgrade_setup(Suite *s)
{
  TCase *tc = tcase_create("upgrade");

  tcase_add_test(tc, test_upgrade);

  suite_add_tcase(s, tc);
}

