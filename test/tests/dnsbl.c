#include "plexus_test.h"

static struct dnsbl_entry *test1, *test2;

static void setup_dnsbl()
{
  struct dnsbl_entry *entry = MyMalloc(sizeof(struct dnsbl_entry));
  strlcpy(entry->name, "test1.rizon.net", sizeof(entry->name));
  dlinkAdd(entry, &entry->node, &dnsbl_items);
  test1 = entry;

  entry = MyMalloc(sizeof(struct dnsbl_entry));
  strlcpy(entry->name, "test2.rizon.net", sizeof(entry->name));
  dlinkAdd(entry, &entry->node, &dnsbl_items);
  test2 = entry;
}

START_TEST(dnsbl_test)
{
  setup_dnsbl();

  struct PlexusClient *client = client_create("test");

  ck_assert(dlink_list_length(&test1->lookups) == 1);
  ck_assert(dlink_list_length(&test2->lookups) == 1);

  struct ip_entry *ip = find_or_add_ip(&client->client->localClient->ip);
  ip->dnsbl_entry = test1;

  timeout_query_list(CurrentTime + 42);
}
END_TEST

void dnsbl_setup(Suite *s)
{
  TCase *tc = tcase_create("dnsbl");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, dnsbl_test);

  suite_add_tcase(s, tc);
}

