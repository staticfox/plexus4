#include "plexus_test.h"

void irc_join(struct PlexusClient *client, const char *channel)
{
  io_write(client, "JOIN %s", channel);
}

void irc_join_key(struct PlexusClient *client, const char *channel, const char *key)
{
  io_write(client, "JOIN %s %s", channel, key);
}

void irc_part(struct PlexusClient *client, const char *channel)
{
  io_write(client, "PART %s :Leaving", channel);
}

void irc_invite(struct PlexusClient *from, struct Channel *chptr, struct Client *to)
{
  io_write(from, "INVITE %s %s", to->name, chptr->chname);
}

void irc_client_set_cmode(struct PlexusClient *client, struct Channel *chptr, const char *modes)
{
  io_write(client, "MODE %s %s", chptr->chname, modes);
}

void irc_client_set_umode(struct PlexusClient *client, const char *modes)
{
  io_write(client, "MODE %s %s", client->client->name, modes);
}

void irc_privmsg(struct PlexusClient *client, const char *target, const char *message)
{
  io_write(client, "PRIVMSG %s :%s", target, message);
}
