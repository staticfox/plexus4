#include "config.h"
#include <stdio.h>
#include <dlfcn.h>
#include <setjmp.h>

#include <openssl/ssl.h>

#ifdef HAVE_LIBJANSSON
#include <jansson.h>

json_t *plexus_upgrade = NULL;
#endif

jmp_buf plexus_upgrade_jmp;

extern void ircd_free_all();

int main(int argc, char **argv)
{
  SSL_library_init();
#ifdef HAVE_LIBJANSSON
  json_decref(json_object());
#endif

  for (;;)
  {
    void *handle;
    union
    {
      int (*f)(int, char **);
      void *ptr;
    } u;

    ircd_free_all();

    handle = dlopen(LIBDIR "/" PACKAGE "/libplexus.so", RTLD_NOW | RTLD_GLOBAL);
    if (!handle)
    {
      fprintf(stderr, "Unable to open libplexus: %s\n", dlerror());
      return -1;
    }

    u.ptr = dlsym(handle, "plexus_main");
    if (!u.ptr)
    {
      fprintf(stderr, "Unable to start libplexus: %s\n", dlerror());
      dlclose(handle);
      return -1;
    }

    if (!setjmp(plexus_upgrade_jmp))
      u.f(argc, argv);

    if (dlclose(handle))
    {
      fprintf(stderr, "Unable to close libplexus: %s\n", dlerror());
      return -1;
    }

    handle = dlopen(LIBDIR "/" PACKAGE "/libplexus.so", RTLD_NOW | RTLD_GLOBAL | RTLD_NOLOAD);
    if (handle)
    {
      fprintf(stderr, "Unable to unload libplexus, aborting\n");
      return -1;
    }
  }
}
